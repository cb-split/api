import os
import os.path

from celery import Celery

import images_splitter

UPLOAD_FOLDER = "/mnt/books"

redis_url = os.environ["QOVERY_DATABASE_CELERY_BROKER_CONNECTION_URI"]
app = Celery("tasks", backend=redis_url, broker=redis_url)


@app.task
def convert(filename):
    print(f"running convert for {filename}...")
    try:
        splitted_filename = images_splitter.splitImages(f"{UPLOAD_FOLDER}/{filename}")
        print(f"{filename} converted to {splitted_filename}")
        delete_file.apply_async((splitted_filename,), countdown=15 * 60)
        return splitted_filename
    finally:
        delete_file.delay(f"{UPLOAD_FOLDER}/{filename}")


@app.task
def delete_file(filename):
    print(f"Deleting {filename}")
    os.remove(filename)
