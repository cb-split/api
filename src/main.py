#!/usr/bin/env python3
import os

import flask
from celery.result import AsyncResult
from flask import abort
from flask import flash
from flask import request
from flask import send_from_directory
from flask import url_for
from flask_cors import CORS
from werkzeug.utils import secure_filename

import tasks

UPLOAD_FOLDER = "/mnt/books"
ALLOWED_EXTENSIONS = {
    "cbr",
}

app = flask.Flask(__name__)
CORS(app)
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
app.config["MAX_CONTENT_LENGTH"] = 200 * 1024 * 1024  # 200 MB


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/upload", methods=["POST"])
def upload_file_json():
    if "file" not in request.files:
        flash("No file part")
        abort(400)
    file = request.files["file"]
    if not file or file.filename == "":
        flash("No selected file")
        abort(400)
    if not allowed_file(file.filename):
        abort(422)
    filename = secure_filename(file.filename)
    file.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
    result = tasks.convert.delay(filename)
    return {"task_url": url_for("task_result", task_id=result.id, filename=filename)}


@app.route("/tasks/<task_id>/<filename>")
def task_result(task_id, filename):
    result = AsyncResult(task_id, app=tasks.app)
    if not result.ready():
        return {"done": False, "original_filename": filename}
    full_path = result.get()
    splitted_filename = full_path.split("/")[-1]
    splitted_download_url = url_for("uploaded_file", filename=splitted_filename)
    return {
        "done": True,
        "original_filename": filename,
        "splitted_filename": splitted_filename,
        "splitted_download_url": splitted_download_url,
    }


@app.route("/uploads/<filename>")
def uploaded_file(filename):
    return send_from_directory(app.config["UPLOAD_FOLDER"], filename)


if __name__ == "__main__":
    print("Server is ready!")
    app.run(host="0.0.0.0", port=5000)
