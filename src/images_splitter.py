import os
import shutil
import tempfile
from contextlib import contextmanager

import rarfile
from PIL import Image


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def splitImages(originalArchivePath, skippedImages, leftToRight):
    with tempfile.TemporaryDirectory() as tmpDir:
        print("reading cbr: %s" % originalArchivePath)
        originalArchive = rarfile.RarFile(originalArchivePath)
        bookDirName = [f.filename for f in originalArchive.infolist() if f.isdir()][0]
        workingDir = os.path.join(tmpDir, bookDirName)
        print("creating working dir %s" % workingDir)
        os.makedirs(os.path.join(tmpDir, workingDir))
        for fileInArchive in originalArchive.infolist():
            if not fileInArchive.isdir():
                splitImage(originalArchive, fileInArchive, workingDir, skippedImages, leftToRight)
        fileNameWithoutExt = os.path.splitext(os.path.basename(originalArchivePath))[0]
        croppedArchiveFileName = "%s-splitted.cbz" % fileNameWithoutExt
        with cd(tmpDir):
            shutil.make_archive(fileNameWithoutExt, "zip", bookDirName)
            os.rename("%s.zip" % fileNameWithoutExt, croppedArchiveFileName)
        croppedArchiveTmpPath = os.path.join(tmpDir, croppedArchiveFileName)
        croppedArchiveFinalPath = os.path.join(os.path.dirname(originalArchivePath), croppedArchiveFileName)
        shutil.move(croppedArchiveTmpPath, croppedArchiveFinalPath)
        return croppedArchiveFinalPath


def splitImage(archive, originalImageFile, croppedImagesDir, nbImagesToSkip, leftToRight):
    originalImage = Image.open(archive.open(originalImageFile))
    width, height = originalImage.size
    name = os.path.basename(originalImageFile.filename)
    number = int(os.path.splitext(name)[0][-3:])
    print("opened image %s" % name)
    if number <= nbImagesToSkip:
        boxAll = (0, 0, width, height)
        crop(originalImage, boxAll, croppedImagesDir, "%03d.jpg" % number)
    else:
        boxLeft = (0, 0, width / 2, height)
        leftPageNumber = (number * 2 - 1) if leftToRight else (number * 2)
        crop(originalImage, boxLeft, croppedImagesDir, "%03d.jpg" % leftPageNumber)
        boxRight = (width / 2, 0, width, height)
        rightPageNumber = (number * 2) if leftToRight else (number * 2 - 1)
        crop(originalImage, boxRight, croppedImagesDir, "%03d.jpg" % rightPageNumber)


def crop(originalImage, box, destDir, croppedFileName):
    croped = originalImage.crop(box)
    cropedFilePath = os.path.join(destDir, croppedFileName)
    croped.save(cropedFilePath)
    print("saved croped file " + cropedFilePath)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("originalFilePath")
    parser.add_argument("--skip", type=int, default=0)
    parser.add_argument("--leftToRight", type=bool, default=False)
    args = parser.parse_args()

    originalFilePath = args.originalFilePath
    nbImagesToSkip = args.skip
    leftToRight = args.leftToRight
    croppedArchiveFinalPath = splitImages(originalFilePath, nbImagesToSkip, leftToRight)
    print("cropped archive written to '%s'" % croppedArchiveFinalPath)
