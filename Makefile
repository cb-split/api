.PHONY: install
install:
	python -m venv venv
	venv/bin/pip install --upgrade pip -r requirements.txt

.PHONY: venv-run
venv-run:
	cd src && ../venv/bin/python -u main.py

.PHONY: build
build:
	docker build . -t comic-book-splitter-back:latest

.PHONY: run
run:
	docker run -ti comic-book-splitter-back:latest
