FROM python:3.9

EXPOSE 5000

RUN \
    sed -i -e's/ main/ main contrib non-free/g' /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y unrar \
    && rm -rf /var/lib/apt/lists/*


COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

RUN mkdir -p /app
COPY src /app

COPY uwsgi.ini /app/uwsgi.ini

COPY entrypoint.sh .

WORKDIR /app

RUN groupadd -g 999 app && useradd -m -r -u 999 -g app app

ENTRYPOINT /entrypoint.sh
