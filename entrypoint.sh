#!/usr/bin/env bash

set -e
set -x

chown -Rv app:app /mnt/books

su app --command "celery -A tasks worker --loglevel=INFO" &

su app --command "uwsgi --ini uwsgi.ini"
